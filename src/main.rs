use std::fs;
use rss::{Channel, Item};
use postmill::Client;
use chrono::prelude::*;

mod urls;

fn post_item(client: &mut Client, item: &Item) {
    client.submit_post("Berkeley_Study_Group",
                       item.link().unwrap(),
                       item.title().unwrap(),
                       &html2md::parse_html(item.content().unwrap())
                       ).unwrap();
}

fn parse_feed(client: &mut Client, url: String, last_datetime: DateTime<FixedOffset>) {
    Channel::from_url(&*url).unwrap().items().into_iter()
        // Only parse items from feeds that have a newer date
        .filter(|item| last_datetime < DateTime::parse_from_rfc2822(item.pub_date().unwrap()).unwrap())
        .for_each(move |item| {
            println!("{} - {}", item.pub_date().unwrap(), item.title().unwrap());
            post_item(client, item);
        });
}

fn main() {
    // The time now
    let now_datetime = Utc::now();

    // The time of the last parse
    let last_datetime_raw = match fs::read_to_string("last_time.txt") {
        Ok(time) => time,
        Err(_) => {
            // Write the current time when no last time is found
            fs::write("last_time.txt", now_datetime.to_rfc3339()).unwrap();
            panic!("No previous time found");
        }
    };
    let last_datetime = DateTime::parse_from_rfc3339(&last_datetime_raw.lines().next().unwrap()).unwrap();

    let mut client = Client::new("https://raddle.me").unwrap();

    // Login
    client.login("StudyBot", include_str!("../password.txt").trim()).unwrap();

    for url in urls::FEEDS {
        parse_feed(&mut client, (*url).into(), last_datetime);
    }

    fs::write("last_time.txt", now_datetime.to_rfc3339()).unwrap();
}
