pub const FEEDS: &[&str] = &[
    "http://sfbay-anarchists.org/feed/"
    /*
    "https://itsgoingdown.org/feed",
    "http://baseandsuperstructure.com/feed/",
    "https://libcom.org/blog/feed",
    "https://sub.media/feed/",
    "http://news.infoshop.org/feed/",
    "https://unicornriot.ninja/feed/",
    "https://325.nostate.net/feed/",
    "https://anarchimedia.com/feed/"
    */
];
